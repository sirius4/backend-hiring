import express from 'express';

export default interface IEndpoint {
  bind(app: express.Application): void;
}
