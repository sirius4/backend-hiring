import express from 'express';

import IEndpoint from '../IEndpoint';
import { injectable } from 'inversify';

@injectable()
class RootEndpoint implements IEndpoint {
  bind(app: express.Application): void {
    app.get('/', (req, res) => {
      res.status(200);
      res.send('Hello world – Backend Hiring task done by Szymon Piechaczek');
      res.end();
    });
  }
}

export default RootEndpoint;
