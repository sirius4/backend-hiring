import IEndpoint from '../IEndpoint';
import express from 'express';
import { inject, injectable } from 'inversify';
import appInjectables from 'application/injectables';
import CreateTestResult, { CreateTestResultInputBoundary } from 'application/usecases/employee/CreateTestResult';
import GetDailyStatistics from 'application/usecases/boss/GetDailyStatistics';
import GetCountryStatistics from '../../../application/usecases/boss/GetCountryStatistics';

@injectable()
class TestResultEndpoint implements IEndpoint {
  @inject(appInjectables.useCases.employee.CreateTestResult)
  private createTestResult!: CreateTestResult;

  postTestResult(req: express.Request, res: express.Response, next: express.NextFunction) {
    const input = new CreateTestResultInputBoundary(req.body || {});
    this.createTestResult
      .execute(input)
      .then((result) => res.status(200).json(result.testResult))
      .catch(next);
  }

  @inject(appInjectables.useCases.boss.GetDailyStatistics)
  private getDailyStatisticsInteractor!: GetDailyStatistics;

  getDailyStatistics(req: express.Request, res: express.Response, next: express.NextFunction) {
    this.getDailyStatisticsInteractor
      .execute()
      .then((result) => res.status(200).json(result))
      .catch(next);
  }

  @inject(appInjectables.useCases.boss.GetCountryStatistics)
  private getCountryStatisticsInteractor!: GetCountryStatistics;

  getCountryStatistics(req: express.Request, res: express.Response, next: express.NextFunction) {
    this.getCountryStatisticsInteractor
      .execute()
      .then((result) => res.status(200).json(result))
      .catch(next);
  }

  bind(app: express.Application): void {
    const router = express.Router();

    router.post('/', this.postTestResult.bind(this));
    router.get('/statistics/daily', this.getDailyStatistics.bind(this));
    router.get('/statistics/country', this.getCountryStatistics.bind(this));

    app.use('/testResult', router);
  }
}

export default TestResultEndpoint;
