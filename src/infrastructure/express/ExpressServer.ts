import IServer from '../IServer';
import { inject, injectable, multiInject } from 'inversify';
import appInjectables from 'application/injectables';
import injectables from './injectables';
import express, { ErrorRequestHandler, Express } from 'express';
import IEndpoint from './IEndpoint';
import bodyParser from 'body-parser';
import { InputBoundaryValidationError } from '../../application/errors';

export interface IExpressServerConfig {
  express: {
    port: number;
  };
}

@injectable()
class ExpressServer implements IServer {
  private config: IExpressServerConfig;

  private readonly _app: Express;

  constructor(
    @inject(appInjectables.Config) config: IExpressServerConfig,
    @multiInject(injectables.Endpoint) endpoints: IEndpoint[],
  ) {
    this.config = config;
    this._app = express();
    this._app.use(
      bodyParser.json({
        type: '*/*',
      }),
    );
    endpoints.forEach((endpoint) => endpoint.bind(this._app));

    // because express requires 4 arguments for an error handler
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const errorHandler: ErrorRequestHandler = (err, req, res, _next) => {
      if (err instanceof InputBoundaryValidationError) {
        return res.status(400).json({
          error: err.message,
          fields: err.validationErrors,
        });
      }

      return res.status(500).json({ error: err.message });
    };
    this._app.use((req, res) => {
      return res.status(404).send();
    });
    this._app.use(errorHandler);
  }

  get app(): express.Express {
    return this._app;
  }

  run(): Promise<void> {
    const { port } = this.config.express;
    return new Promise((resolve) => {
      this._app.listen(port, resolve);
    });
  }
}

export default ExpressServer;
