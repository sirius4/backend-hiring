import { ContainerModule } from 'inversify';
import expressInjectables from './injectables';
import infrastructureInjectables from 'infrastructure/injectables';
import IServer from 'infrastructure/IServer';
import IEndpoint from './IEndpoint';
import RootEndpoint from './endpoints/RootEndpoint';
import ExpressServer from './ExpressServer';
import TestResultEndpoint from './endpoints/TestResultEndpoint';

export default new ContainerModule((bind) => {
  bind<IServer>(infrastructureInjectables.Server).to(ExpressServer);
  bind<IEndpoint>(expressInjectables.Endpoint).to(RootEndpoint);
  bind<IEndpoint>(expressInjectables.Endpoint).to(TestResultEndpoint);
});
