export default interface IServer {
  run(): Promise<void>;
}
