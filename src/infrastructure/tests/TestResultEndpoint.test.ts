import container from '../../container';
import ExpressServer from '../express/ExpressServer';
import request from 'supertest';
import infrastructureInjectables from 'infrastructure/injectables';
import IDatabase from '../IDatabase';
import TestResult, { TestResultOutcome } from '../../domain/entities/TestResult';
import { isSameDay, parseISO } from 'date-fns';
import { GeoJSON } from '../mongodb/models/types';
import mongoose from 'mongoose';
import { ITestResultDoc } from '../mongodb/models/TestResultModel';

const ObjectIdRegex = /^[a-f\d]{24}$/i;
const ISODateRegex = /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+/;

const TestResultModel = mongoose.models.testResult as mongoose.Model<ITestResultDoc>;

const statsData: (Omit<TestResult, 'location'> & { location: GeoJSON })[] = [
  {
    country: 'PL',
    location: {
      type: 'Point',
      coordinates: [50.061785037850555, 19.936854094370066],
    },
    outcome: TestResultOutcome.POSITIVE,
    age: 22,
    patientName: 'Test Testowsky 1',
    createdAt: new Date(2021, 10, 21, 19, 20),
  },
  {
    country: 'PL',
    location: {
      type: 'Point',
      coordinates: [50.061785037850555, 19.936854094370066],
    },
    outcome: TestResultOutcome.NEGATIVE,
    age: 22,
    patientName: 'Test Testowsky 2',
    createdAt: new Date(2021, 10, 20, 18, 35),
  },
  {
    country: 'US',
    location: {
      type: 'Point',
      coordinates: [50.061785037850555, 19.936854094370066],
    },
    outcome: TestResultOutcome.POSITIVE,
    age: 22,
    patientName: 'Test Testowsky 3',
    createdAt: new Date(2021, 10, 21, 18, 35),
  },
  {
    country: 'US',
    location: {
      type: 'Point',
      coordinates: [50.061785037850555, 19.936854094370066],
    },
    outcome: TestResultOutcome.NEGATIVE,
    age: 22,
    patientName: 'Test Testowsky 4',
    createdAt: new Date(2021, 10, 21, 18, 35),
  },
];

describe('/testResult', () => {
  const app = container.get<ExpressServer>(infrastructureInjectables.Server).app;
  const database = container.get<IDatabase>(infrastructureInjectables.Database);

  beforeAll(async () => {
    await database.connect();
  });

  beforeEach(async () => {
    await TestResultModel.deleteMany({});
  });

  describe('/ POST', () => {
    it('should return created entity', (done) => {
      const testResult = {
        patientName: 'Test Testowsky',
        age: 27,
        location: [50.061785037850555, 19.936854094370066],
        outcome: 'negative',
        country: 'PL',
      };
      request(app)
        .post('/testResult')
        .send(testResult)
        .expect(200)
        .expect('Content-Type', /json/)
        .then((response) => {
          expect(response.body).toMatchObject(testResult);
          expect(response.body.id).toMatch(ObjectIdRegex);
          expect(response.body.createdAt).toMatch(ISODateRegex);
          done();
        })
        .catch((err) => done(err));
    });

    it('should return validation_error for incorrect outcome', (done) => {
      const testResult = {
        patientName: 'Test Testowsky',
        age: 27,
        location: [50.061785037850555, 19.936854094370066],
        outcome: 'unknown',
        country: 'PL',
      };
      request(app)
        .post('/testResult')
        .send(testResult)
        .expect(400)
        .expect('Content-Type', /json/)
        .then((response) => {
          expect(response.body).toMatchObject({
            error: 'validation_error',
          });
          done();
        })
        .catch((err) => done(err));
    });

    it('should return validation_error for incorrect location', (done) => {
      const testResult = {
        patientName: 'Test Testowsky',
        age: 27,
        location: [50.061785037850555, 'abc'],
        outcome: 'unknown',
        country: 'PL',
      };
      request(app)
        .post('/testResult')
        .send(testResult)
        .expect(400)
        .expect('Content-Type', /json/)
        .then((response) => {
          expect(response.body).toMatchObject({
            error: 'validation_error',
          });
          done();
        })
        .catch((err) => done(err));
    });
    it('should return validation_error for incorrect location coordinates', (done) => {
      const testResult = {
        patientName: 'Test Testowsky',
        age: 27,
        location: [50.061785037850555],
        outcome: 'unknown',
        country: 'PL',
      };
      request(app)
        .post('/testResult')
        .send(testResult)
        .expect(400)
        .expect('Content-Type', /json/)
        .then((response) => {
          expect(response.body).toMatchObject({
            error: 'validation_error',
          });
          done();
        })
        .catch((err) => done(err));
    });
    it('should return validation_error for incorrect country', (done) => {
      const testResult = {
        patientName: 'Test Testowsky',
        age: 27,
        location: [50.061785037850555, 'abc'],
        outcome: 'unknown',
        country: 'UNK',
      };
      request(app)
        .post('/testResult')
        .send(testResult)
        .expect(400)
        .expect('Content-Type', /json/)
        .then((response) => {
          expect(response.body).toMatchObject({
            error: 'validation_error',
          });
          done();
        })
        .catch((err) => done(err));
    });
  });

  describe('/statistics/daily GET', () => {
    it('should return total and positive values for each day', (done) => {
      TestResultModel.insertMany(statsData)
        .then(() => request(app).get('/testResult/statistics/daily').expect(200).expect('Content-Type', /json/))
        .then((response) => {
          expect(response.body).toHaveProperty('days');
          expect(response.body.days).toHaveLength(2);
          expect(isSameDay(parseISO(response.body.days[0].day), new Date(2021, 10, 20))).toEqual(true);
          expect(response.body.days[0]).toMatchObject({
            total: 1,
            positive: 0,
          });
          expect(isSameDay(parseISO(response.body.days[1].day), new Date(2021, 10, 21))).toEqual(true);
          expect(response.body.days[1]).toMatchObject({
            total: 3,
            positive: 2,
          });

          done();
        })
        .catch((err) => done(err));
    });
  });

  describe('/statistics/country GET', () => {
    it('should return total and positive values for each country', (done) => {
      TestResultModel.insertMany(statsData)
        .then(() => request(app).get('/testResult/statistics/country').expect(200).expect('Content-Type', /json/))
        .then((response) => {
          expect(response.body).toHaveProperty('countries');
          expect(response.body.countries).toHaveLength(2);
          expect(response.body.countries[0]).toMatchObject({
            total: 2,
            positive: 1,
            country: 'PL',
          });
          expect(response.body.countries[1]).toMatchObject({
            total: 2,
            positive: 1,
            country: 'US',
          });

          done();
        })
        .catch((err) => done(err));
    });
  });
});
