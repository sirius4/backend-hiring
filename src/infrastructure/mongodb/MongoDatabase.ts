import mongoose from 'mongoose';
import { inject, injectable } from 'inversify';
import appInjectables from 'application/injectables';
import IDatabase from 'infrastructure/IDatabase';

export interface IMongooseDatabaseConfig {
  database: {
    url: string;
  };
}

@injectable()
export default class MongoDatabase implements IDatabase {
  @inject(appInjectables.Config)
  private config!: IMongooseDatabaseConfig;

  private moongoose: typeof mongoose | undefined;

  async connect(): Promise<void> {
    this.moongoose = await mongoose.connect(this.config.database.url, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
    });
  }
}
