import { ContainerModule } from 'inversify';
import domainInjectables from 'domain/injectables';
import infrastructureInjectables from 'infrastructure/injectables';
import IDatabase from '../IDatabase';
import MongoDatabase from './MongoDatabase';

import { ITestResultRepository } from 'domain/repositories/ITestResultRepository';
import TestResultRepository from './repository/TestResultRepository';

export default new ContainerModule((bind) => {
  bind<IDatabase>(infrastructureInjectables.Database).to(MongoDatabase);
  bind<ITestResultRepository>(domainInjectables.TestResultRepository).toConstantValue(TestResultRepository);
});
