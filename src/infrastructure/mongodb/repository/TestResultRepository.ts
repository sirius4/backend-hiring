import {
  CountryCountResults,
  DailyCountResults,
  ITestResultRepository,
} from 'domain/repositories/ITestResultRepository';
import TestResult, { TestResultOutcome } from 'domain/entities/TestResult';
import { Entity } from 'domain/types';
import TestResultModel, { ITestResultDoc } from '../models/TestResultModel';

function mapDocToEntity(doc: ITestResultDoc): Entity<TestResult> {
  return {
    id: String(doc._id),
    age: doc.age,
    outcome: doc.outcome,
    location: doc.location?.coordinates,
    patientName: doc.patientName,
    country: doc.country,
    createdAt: doc.createdAt,
  };
}

const TestResultRepository: ITestResultRepository = {
  async countResultsByCountry(): Promise<CountryCountResults[]> {
    return TestResultModel.aggregate<CountryCountResults>([
      {
        $addFields: {
          positiveValue: { $cond: [{ $eq: ['$outcome', TestResultOutcome.POSITIVE] }, 1, 0] },
        },
      },
      {
        $group: {
          _id: '$country',
          total: { $sum: 1 },
          positive: { $sum: '$positiveValue' },
        },
      },
      {
        $project: {
          _id: 0,
          country: '$_id',
          total: 1,
          positive: 1,
        },
      },
      { $sort: { country: 1 } },
    ]);
  },

  async countResultsByDay(): Promise<DailyCountResults[]> {
    return TestResultModel.aggregate<DailyCountResults>([
      {
        $addFields: {
          positiveValue: { $cond: [{ $eq: ['$outcome', TestResultOutcome.POSITIVE] }, 1, 0] },
          date: {
            $dateFromParts: {
              year: { $year: '$createdAt' },
              month: { $month: '$createdAt' },
              day: { $dayOfMonth: '$createdAt' },
            },
          },
        },
      },
      {
        $group: {
          _id: '$date',
          total: { $sum: 1 },
          positive: { $sum: '$positiveValue' },
        },
      },
      {
        $project: {
          _id: 0,
          day: '$_id',
          total: 1,
          positive: 1,
        },
      },
      { $sort: { day: 1 } },
    ]);
  },

  async create(testResult: TestResult): Promise<Entity<TestResult>> {
    const doc = new TestResultModel({
      ...testResult,
      location: {
        type: 'Point',
        coordinates: testResult.location,
      },
    });
    await doc.save();

    return mapDocToEntity(doc);
  },
};

export default TestResultRepository;
