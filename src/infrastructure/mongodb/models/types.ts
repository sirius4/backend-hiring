import { Schema } from 'mongoose';

export interface GeoJSON {
  type: 'Point';
  coordinates: [number, number];
}

const GeoJSONFields: Record<'type' | 'coordinates', any> = {
  type: {
    type: Schema.Types.String,
    required: true,
    enum: 'Point',
  },
  coordinates: {
    type: [Schema.Types.Number],
    required: true,
    minLength: 2,
    maxLength: 2,
  },
};

export const GeoJSONSchema = new Schema(GeoJSONFields);

export default {
  GeoJSONSchema,
};
