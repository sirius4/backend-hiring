import mongoose, { model, Schema, Document } from 'mongoose';
import TestResult, { TestResultOutcome } from 'domain/entities/TestResult';
import { GeoJSONSchema, GeoJSON } from './types';

export interface ITestResultModel extends TestResult {}
export interface ITestResultDoc extends Omit<ITestResultModel, 'location'>, Document {
  location: GeoJSON;
}

const testResultFields: Record<keyof ITestResultModel, any> = {
  patientName: {
    type: Schema.Types.String,
    required: true,
    maxLength: 256,
    index: true,
  },
  age: {
    type: Schema.Types.Number,
    required: true,
  },
  location: {
    type: GeoJSONSchema,
    required: true,
  },
  outcome: {
    type: Schema.Types.String,
    enum: Object.values(TestResultOutcome),
    required: true,
  },
  country: {
    type: Schema.Types.String,
    required: true,
    minLength: 2,
    maxLength: 2,
  },
  createdAt: {
    type: Schema.Types.Date,
    required: true,
  },
};

const TestResultSchema = new Schema(testResultFields);

const TestResultModel =
  (mongoose.models.testResult as mongoose.Model<ITestResultDoc> | undefined) ||
  model<ITestResultDoc>('testResult', TestResultSchema);

export default TestResultModel;
