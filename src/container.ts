import 'reflect-metadata';
import { Container } from 'inversify';
import appInjectables from 'application/injectables';

import MongoDBContainer from 'infrastructure/mongodb/container';
import ExpressContainer from 'infrastructure/express/container';
import ApplicationContainer from 'application/container';
import config, { IMainConfig } from './config';

const container = new Container();
container.bind<IMainConfig>(appInjectables.Config).toConstantValue(config);

container.load(MongoDBContainer);
container.load(ExpressContainer);
container.load(ApplicationContainer);

export default container;
