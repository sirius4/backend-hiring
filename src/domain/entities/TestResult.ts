import { GeoPoint } from '../types';

export enum TestResultOutcome {
  NEGATIVE = 'negative',
  POSITIVE = 'positive',
}

export default interface TestResult {
  patientName: string;
  outcome: TestResultOutcome;
  age: number;
  location: GeoPoint;
  country: string;
  createdAt: Date;
}
