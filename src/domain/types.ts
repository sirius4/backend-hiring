export type UUID = string;
export type ID = UUID;

export interface Indexed {
  id: ID;
}

export type Entity<T> = T & Indexed;

export type GeoPoint = [number, number];
