import TestResult from 'domain/entities/TestResult';
import { Entity } from '../types';

export type DailyCountResults = {
  day: Date;
  total: number;
  positive: number;
};

export type CountryCountResults = {
  country: string;
  total: number;
  positive: number;
};

export interface ITestResultRepository {
  create(testResult: TestResult): Promise<Entity<TestResult>>;
  countResultsByDay(): Promise<DailyCountResults[]>;
  countResultsByCountry(): Promise<CountryCountResults[]>;
}
