import { config as dotenvConfig } from 'dotenv';
import { IMongooseDatabaseConfig } from './infrastructure/mongodb/MongoDatabase';
import { IExpressServerConfig } from './infrastructure/express/ExpressServer';

dotenvConfig();

export interface IMainConfig extends IMongooseDatabaseConfig, IExpressServerConfig {}

const config: IMainConfig = {
  express: {
    port: Number(process.env.PORT) || 4000,
  },
  database: {
    url: 'mongodb://treeline:treeline@localhost:27017/jshiring',
  },
};

export default config;
