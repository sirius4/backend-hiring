import container from './container';
import infrastructureInjectables from './infrastructure/injectables';
import IServer from './infrastructure/IServer';
import IDatabase from './infrastructure/IDatabase';

const server = container.get<IServer>(infrastructureInjectables.Server);
const database = container.get<IDatabase>(infrastructureInjectables.Database);

(async () => {
  await database.connect();
  await server.run();
  console.log('Server started!');
})().catch((error) => {
  console.error('Server start failed', error);
});
