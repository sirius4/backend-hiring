import { Container } from 'inversify';
import { isAfter } from 'date-fns';
import domainInjectables from 'domain/injectables';
import appInjectables from 'application/injectables';
import { ITestResultRepository } from 'domain/repositories/ITestResultRepository';
import CreateTestResult, { CreateTestResultInputBoundary } from './CreateTestResult';
import { ID } from 'domain/types';
import { TestResultOutcome } from 'domain/entities/TestResult';

describe('Employee.CreateTestResult', () => {
  const container = new Container();
  const assignedId: ID = '619a6735d10e63480889a4dc';

  const testResultRepositoryMock: ITestResultRepository = {
    create: jest.fn((data) =>
      Promise.resolve({
        ...data,
        id: assignedId,
      }),
    ),
    countResultsByDay: jest.fn(),
    countResultsByCountry: jest.fn(),
  };

  beforeAll(() => {
    container
      .bind<ITestResultRepository>(domainInjectables.TestResultRepository)
      .toConstantValue(testResultRepositoryMock);
    container.bind<CreateTestResult>(appInjectables.useCases.employee.CreateTestResult).to(CreateTestResult);
  });

  it('returns entity created with repository', async () => {
    expect.assertions(3);
    const input = new CreateTestResultInputBoundary({
      country: 'PL',
      location: [50.061785037850555, 19.936854094370066],
      outcome: TestResultOutcome.POSITIVE,
      patientName: 'Test Testowsky',
      age: 25,
    });

    const interactor = container.get<CreateTestResult>(appInjectables.useCases.employee.CreateTestResult);
    const { testResult } = await interactor.execute(input);

    expect(testResult.id).toEqual(assignedId);
    expect(!isAfter(testResult.createdAt, new Date())).toEqual(true);
    expect(testResult).toMatchObject(input);
  });
});
