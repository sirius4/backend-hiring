import ValidatedInputBoundary from 'application/abstract/ValidatedInputBoundary';
import { ArrayIsCoordinate } from 'application/abstract/validators/geo';
import { IsEnum, IsISO31661Alpha2, IsNumber, IsPositive, IsString, Length } from 'class-validator';
import TestResult, { TestResultOutcome } from 'domain/entities/TestResult';
import { Entity, GeoPoint } from 'domain/types';
import { IOutputBoundary } from 'application/abstract';
import ValidatedInteractor from 'application/abstract/ValidatedInteractor';
import { inject, injectable } from 'inversify';
import { ITestResultRepository } from 'domain/repositories/ITestResultRepository';
import domainInjectables from 'domain/injectables';

type TestResultCreationData = Omit<TestResult, 'createdAt'>;

export class CreateTestResultInputBoundary extends ValidatedInputBoundary implements TestResultCreationData {
  @IsString()
  @Length(1, 256)
  patientName: string;

  @IsNumber()
  @IsPositive()
  age: number;

  @ArrayIsCoordinate()
  location: GeoPoint;

  @IsEnum(TestResultOutcome)
  outcome: TestResultOutcome;

  @IsISO31661Alpha2()
  country: string;

  constructor(data: TestResultCreationData) {
    super();
    this.patientName = data.patientName;
    this.country = data.country;
    this.age = data.age;
    this.outcome = data.outcome;
    this.location = data.location;
  }
}

export interface CreateTestResultOutputBoundary extends IOutputBoundary {
  testResult: Entity<TestResult>;
}

/* Just a simple "create" Use Case for Test Result */
@injectable()
class CreateTestResult extends ValidatedInteractor<CreateTestResultInputBoundary, CreateTestResultOutputBoundary> {
  @inject(domainInjectables.TestResultRepository)
  private testResultRepository!: ITestResultRepository;

  protected async executeValidated(input: CreateTestResultInputBoundary): Promise<CreateTestResultOutputBoundary> {
    const testResult = await this.testResultRepository.create({
      ...input,
      createdAt: new Date(),
    });
    return {
      testResult,
    };
  }
}

export default CreateTestResult;
