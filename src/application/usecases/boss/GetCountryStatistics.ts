import { IInteractor, IOutputBoundary } from '../../abstract';
import { inject, injectable } from 'inversify';
import { ITestResultRepository } from 'domain/repositories/ITestResultRepository';
import domainInjectables from 'domain/injectables';

export interface GetCountryStatisticsOutputBoundary extends IOutputBoundary {
  countries: {
    country: string;
    total: number;
    positive: number;
  }[];
}

@injectable()
class GetCountryStatistics implements IInteractor<{}, GetCountryStatisticsOutputBoundary> {
  @inject(domainInjectables.TestResultRepository)
  private testResultRepository: ITestResultRepository;

  async execute(): Promise<GetCountryStatisticsOutputBoundary> {
    const results = await this.testResultRepository.countResultsByCountry();

    return {
      countries: results,
    };
  }
}

export default GetCountryStatistics;
