import { IInteractor, IOutputBoundary } from '../../abstract';
import { inject, injectable } from 'inversify';
import { ITestResultRepository } from 'domain/repositories/ITestResultRepository';
import domainInjectables from 'domain/injectables';

export interface GetDailyStatisticsOutputBoundary extends IOutputBoundary {
  days: {
    day: Date;
    total: number;
    positive: number;
  }[];
}

@injectable()
class GetDailyStatistics implements IInteractor<{}, GetDailyStatisticsOutputBoundary> {
  @inject(domainInjectables.TestResultRepository)
  private testResultRepository: ITestResultRepository;

  async execute(): Promise<GetDailyStatisticsOutputBoundary> {
    const results = await this.testResultRepository.countResultsByDay();

    return {
      days: results,
    };
  }
}

export default GetDailyStatistics;
