import { Container } from 'inversify';
import domainInjectables from 'domain/injectables';
import appInjectables from 'application/injectables';
import { ITestResultRepository } from 'domain/repositories/ITestResultRepository';
import GetDailyStatistics from './GetDailyStatistics';

describe('Boss.GetDailyStatistics', () => {
  const container = new Container();

  const testResultRepositoryMock: ITestResultRepository = {
    create: jest.fn(),
    countResultsByDay: jest.fn(),
    countResultsByCountry: jest.fn(),
  };

  beforeAll(() => {
    container
      .bind<ITestResultRepository>(domainInjectables.TestResultRepository)
      .toConstantValue(testResultRepositoryMock);
    container.bind<GetDailyStatistics>(appInjectables.useCases.boss.GetDailyStatistics).to(GetDailyStatistics);
  });

  it('returns statistics calculated by repository', async () => {
    expect.assertions(3);
    const mockDays = [
      {
        day: new Date(2021, 10, 20),
        total: 25,
        positive: 3,
      },
      {
        day: new Date(2021, 10, 21),
        total: 20,
        positive: 4,
      },
    ];

    testResultRepositoryMock.countResultsByDay = jest.fn(() => Promise.resolve(mockDays));

    const interactor = container.get<GetDailyStatistics>(appInjectables.useCases.boss.GetDailyStatistics);
    const { days } = await interactor.execute();

    expect(days).toHaveLength(2);
    expect(days[0]).toMatchObject(mockDays[0]);
    expect(days[1]).toMatchObject(mockDays[1]);
  });
});
