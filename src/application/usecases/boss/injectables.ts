export default {
  GetDailyStatistics: Symbol('GetDailyStatistics'),
  GetCountryStatistics: Symbol('GetCountryStatistics'),
};
