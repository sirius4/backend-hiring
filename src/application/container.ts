import { ContainerModule } from 'inversify';
import injectables from './injectables';
import CreateTestResult from './usecases/employee/CreateTestResult';
import GetDailyStatistics from './usecases/boss/GetDailyStatistics';
import GetCountryStatistics from './usecases/boss/GetCountryStatistics';

export default new ContainerModule((bind) => {
  bind<CreateTestResult>(injectables.useCases.employee.CreateTestResult).to(CreateTestResult);
  bind<GetDailyStatistics>(injectables.useCases.boss.GetDailyStatistics).to(GetDailyStatistics);
  bind<GetCountryStatistics>(injectables.useCases.boss.GetCountryStatistics).to(GetCountryStatistics);
});
