import { ValidationError } from 'class-validator';

export class ApplicationError extends Error {}

export class InputBoundaryValidationError extends ApplicationError {
  readonly validationErrors: ValidationError[];

  constructor(message: string, validationErrors: ValidationError[]) {
    super(message);
    this.validationErrors = validationErrors;
  }
}

export default {
  ApplicationError,
  InputBoundaryValidationError,
};
