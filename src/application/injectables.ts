import 'reflect-metadata';
import bossInjectables from './usecases/boss/injectables';
import employeeInjectables from './usecases/employee/injectables';

export default {
  Config: Symbol('Config'),
  useCases: {
    boss: bossInjectables,
    employee: employeeInjectables,
  },
};
