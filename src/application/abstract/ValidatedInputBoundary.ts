import { validate } from 'class-validator';
import { IInputBoundary } from './index';
import { InputBoundaryValidationError } from '../errors';

const defaultValidationOptions = {
  validationError: { target: false },
};

const VALIDATION_ERROR_MESSAGE = 'validation_error';

export default abstract class ValidatedInputBoundary implements IInputBoundary {
  async validate(): Promise<void> {
    const errors = await validate(this, defaultValidationOptions);
    if (errors.length > 0) {
      throw new InputBoundaryValidationError(VALIDATION_ERROR_MESSAGE, errors);
    }
  }
}
