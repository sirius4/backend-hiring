import { injectable } from 'inversify';
import { IInteractor } from './index';
import ValidatedInputBoundary from './ValidatedInputBoundary';

@injectable()
export default abstract class ValidatedInteractor<InputT extends ValidatedInputBoundary, OutputT>
  implements IInteractor<InputT, OutputT>
{
  execute(input: InputT): Promise<OutputT> {
    return input.validate().then(() => this.executeValidated(input));
  }

  protected abstract executeValidated(input: InputT): Promise<OutputT>;
}
