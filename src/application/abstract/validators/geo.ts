import { ValidationOptions, registerDecorator } from 'class-validator';
import validator from 'validator';

export const ARRAY_IS_COORDINATE = 'arrayIsCoordinate';

export function isLatLong(value: string): boolean {
  return typeof value === 'string' && validator.isLatLong(value);
}

export function isLongitude(value: number): boolean {
  return typeof value === 'number' && isLatLong(`0,${value}`);
}

export function isLatitude(value: number): boolean {
  return typeof value === 'number' && isLatLong(`${value},0`);
}

export function arrayIsCoordinate(values: any[]) {
  if (!Array.isArray(values)) {
    return false;
  }
  if (values.length !== 2) {
    return false;
  }
  const isValidLongitude = isLongitude(values[0]);
  const isValidLatitude = isLatitude(values[1]);
  return isValidLatitude && isValidLongitude;
}

export function ArrayIsCoordinate(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: ARRAY_IS_COORDINATE,
      target: object.constructor,
      propertyName,
      constraints: [],
      options: validationOptions,
      validator: {
        validate(value: any) {
          return arrayIsCoordinate(value);
        },
      },
    });
  };
}
