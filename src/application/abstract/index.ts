export interface IInputBoundary {}
export interface IOutputBoundary {}

export interface IInteractor<InputT extends IInputBoundary, OutputT extends IOutputBoundary | void> {
  execute(input: InputT): Promise<OutputT>;
}
