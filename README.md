# Backend hiring Treeline
Solution by Szymon Piechaczek

## Running
```shell
docker-compose up
npm start
```

## Tests
```shell
npm run tests
```
Notice: integration test clear the database.

## API

### Create test result:
```
POST /testResult
{
    "patientName": "Test",
    "location": [50.061785037850555, 19.936854094370066],
    "outcome": "negative",
    "country": "PL",
    "age": 33
}
```

### Get statistics per day:
```
POST /testResult/statistics/daily
```
### Get statistics per country:
```
POST /testResult/statistics/country
```